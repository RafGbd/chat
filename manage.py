import asyncio
import argparse
import asyncio_redis
import settings
from sqlalchemy.schema import CreateTable
from aiopg.sa import create_engine
from app.models import (metadata, users_table, hash_password,
                        match_stats_table, chat_logs_table)

@asyncio.coroutine
def clear_redis(args):
    connection = yield from asyncio_redis.Connection.create(host='127.0.0.1', port=6379)
    yield from connection.delete(['premium_queue', 'regular_queue'])
    connection.close()

@asyncio.coroutine
def syncdb(args):
    engine = yield from create_engine(user=settings.DB_USER,
                                      database=settings.DB_NAME,
                                      host=settings.DB_HOST,
                                      password=settings.DB_PASSWORD)
    with (yield from engine) as conn:
        yield from conn.execute(CreateTable(users_table))
        yield from conn.execute(CreateTable(match_stats_table))
        yield from conn.execute(CreateTable(chat_logs_table))

@asyncio.coroutine
def createuser(args):
    engine = yield from create_engine(user=settings.DB_USER,
                                      database=settings.DB_NAME,
                                      host=settings.DB_HOST,
                                      password=settings.DB_PASSWORD)
    with (yield from engine) as conn:
        yield from conn.execute(users_table.insert().values(
            username=args.username,
            password=hash_password(args.password)
        ))

@asyncio.coroutine
def deleteuser(args):
    engine = yield from create_engine(user=settings.DB_USER,
                                      database=settings.DB_NAME,
                                      host=settings.DB_HOST,
                                      password=settings.DB_PASSWORD)
    with (yield from engine) as conn:
        yield from conn.execute(
            users_table.delete(users_table.c.username == args.username)
        )
        res = yield from conn.execute(users_table.select())
        for row in res:
            print(row.id, row.username)


parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers(dest='cmd')
subparsers.required = True

syncdb_parser = subparsers.add_parser('syncdb', help="creates app's tables, if they don't exist")
syncdb_parser.set_defaults(func=syncdb)

createuser_parser = subparsers.add_parser('createuser', help="creates user with access to dashboard")
createuser_parser.add_argument('--username', type=str)
createuser_parser.add_argument('--password', type=str)
createuser_parser.set_defaults(func=createuser)


deleteuser_parser = subparsers.add_parser('deleteuser', help="deletes user")
deleteuser_parser.add_argument('--username', type=str)
deleteuser_parser.set_defaults(func=deleteuser)

clear_redis_parser = subparsers.add_parser('clear_redis', help="clear redis")
clear_redis_parser.set_defaults(func=clear_redis)

if __name__ == '__main__':
    cmd = parser.parse_args()
    asyncio.get_event_loop().run_until_complete(cmd.func(cmd))
