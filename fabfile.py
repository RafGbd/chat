import os
from fabric.api import *
import fabtools

postgres_db_name = 'chat_db'
postgres_user_name = 'chat'
postgres_password = '111ff111'


root_dir = '/opt/chat_service'
venv_dir = '/opt/chat_service/venv/'
prject_dir = '/opt/chat_service/chat'

pyenv_dir = '/opt/pyenv'
pyenv_executable = '/opt/pyenv/bin/pyenv'
python_version = '3.5.1'
python_dir = '/opt/pyenv/versions/%s' % python_version

gunicorn_workers = '19'

pyenv = lambda command: 'PYENV_ROOT=%s %s %s' % (pyenv_dir, pyenv_executable, command)

@task
def dev():
    env.user = 'vagrant'
    env.password = 'vagrant'
    env.hosts = ['192.168.0.20']

# @task
# def prod():
#     env.user = 'root'
#     env.password = 'F4dF3dApp@123'
#     env.hosts = ['46.101.219.142']

@task
def prod():
    env.user = 'root'
    env.password = '111fg111'
    env.hosts = ['46.101.186.208']



def setup_pyenv():
    fabtools.require.deb.packages([
        'git',
        'make',
        'build-essential',
        'libssl-dev',
        'zlib1g-dev',
        'libbz2-dev',
        'libreadline-dev',
        'libsqlite3-dev',
        'wget',
        'curl',
        'llvm',
    ])
    fabtools.require.git.working_copy(
        'git://github.com/yyuu/pyenv.git', path=pyenv_dir, use_sudo=True
    )
    sudo(pyenv('init -'))
    sudo(pyenv('--version'))
    sudo(pyenv('install 3.5.1 -v -s'))
    sudo(pyenv('rehash'))

@task
def setup_venv():
    fabtools.require.deb.packages([
        'git',
        'python-dev',
        'libpq-dev',
    ])
    """
        usage: fab dev setup_venv
        installs virtualenv package,
        creates virtual enviroment for chat service
    """
    setup_pyenv()
    fabtools.require.deb.package('python-virtualenv')
    python_executable = python_dir + "/bin/python"
    fabtools.require.python.virtualenv(
        venv_dir,
        use_sudo=True,
        user='root', venv_python=python_executable)
    with fabtools.python.virtualenv(venv_dir):
        fabtools.require.python.package('gunicorn', use_sudo=True)
        fabtools.require.python.package('ujson', use_sudo=True)
        fabtools.require.python.package('aiohttp', use_sudo=True)
        fabtools.require.python.package('cryptography', use_sudo=True)
        fabtools.require.python.package('aiohttp_session', use_sudo=True)
        fabtools.require.python.package('sqlalchemy', use_sudo=True)
        fabtools.require.python.package('aiopg', use_sudo=True)
        fabtools.require.python.package('aiohttp_jinja2', use_sudo=True)
        fabtools.require.python.package('asyncio_redis', use_sudo=True)
        fabtools.require.python.package('python-dateutil', use_sudo=True)
        
@task
def setup_guincorn():

    gunicorn_exe = os.path.join(venv_dir, 'bin/gunicorn')
    fabtools.require.supervisor.process('gunicorn',
        command='%(gunicorn_exe)s server:app --bind 127.0.0.1:8088 \
        --worker-class==worker.GunicornChatWebWorker \
        --workers=%(gunicorn_workers)s' % {
            'gunicorn_exe': gunicorn_exe,
            'gunicorn_workers': gunicorn_workers
        },
        directory=prject_dir,
        autostart=True,
        autorestart=True,
        user='www-data',
        environment="PYTHONPATH='${PYTHONPATH}:/opt/chat_service/chat/'"
    )

@task
def setup_redis():
    """
        usage: fab dev setup_redis
        installs redis package
    """
    fabtools.require.redis.instance('chat')

@task
def setup_postgres():
    """
        usage: fab dev setup_postgres
        installs postgresql package,
        creates database and tables
    """
    fabtools.require.postgres.server()
    fabtools.require.postgres.user(
        postgres_user_name, password=postgres_password,
        createdb=True, createrole=True
    )
    fabtools.require.postgres.database(postgres_db_name, owner=postgres_user_name)

@task
def setup_nginx():
    fabtools.require.nginx.server()
    fabtools.files.remove('/etc/nginx/sites-enabled/default', recursive=False, use_sudo=True)

    CONFIG_TPL = '''
    # timeout 86400s;
    proxy_read_timeout 86400s;
    proxy_send_timeout 86400s;

    upstream app_server {
        server 127.0.0.1:8088 fail_timeout=0;
    }

    map $http_upgrade $connection_upgrade {
        default upgrade;
        ''      close;
    }
    server {
        listen %(port)d default_server;
        client_max_body_size 4G;

        # set the correct host(s) for your site
        # server_name %(server_name)s %(server_alias)s;

        access_log  /var/log/nginx/%(server_name)s.log;

        keepalive_timeout 5;



        location / {
          # checks for static file, if not found proxy to app
          try_files $uri @proxy_to_app;
        }

        location @proxy_to_app {
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
          # enable this if and only if you use HTTPS
          # proxy_set_header X-Forwarded-Proto https;
          proxy_set_header Host $http_host;
          proxy_http_version 1.1;
          proxy_set_header Upgrade $http_upgrade;
          proxy_set_header Connection $connection_upgrade;

          proxy_redirect off;
          proxy_pass http://app_server;
        }

        error_page 500 502 503 504 /500.html;
        location = /500.html {
          root /path/to/app/current/public;
        }
      }
    '''

    fabtools.require.nginx.site('chat_service',
        template_contents=CONFIG_TPL,
        port=80,
        server_alias='www.example.com',
    )

@task
def prepare():
    """
        usage: fab dev prepare
        installs postgresql, redis, gunicorn, supervisor, nginx 
        creates virtualenv, db tables, supervisor config
    """
    fabtools.require.deb.uptodate_index(max_age={'day': 1})
    setup_venv()
    setup_postgres()
    setup_redis()
    setup_guincorn()
    setup_nginx()