import asyncio
from aiohttp.worker import GunicornWebWorker

class GunicornChatWebWorker(GunicornWebWorker):
    @asyncio.coroutine
    def close(self):
        import asyncio_redis
        connection = yield from asyncio_redis.Connection.create(host='127.0.0.1', port=6379)
        yield from connection.delete(['premium_queue', 'regular_queue', 'users_stats'])
        connection.close()

        if self.servers:
            servers = self.servers
            self.servers = None

            # stop accepting connections
            for server, handler in servers.items():
                self.log.info("Stopping server: %s, connections: %s",
                              self.pid, len(handler.connections))
                server.close()

            # stop alive connections
            tasks = [
                handler.finish_connections(
                    timeout=self.cfg.graceful_timeout / 100 * 95)
                for handler in servers.values()]
            yield from asyncio.wait(tasks, loop=self.loop)

            # stop application
            yield from self.wsgi.finish()