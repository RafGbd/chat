# -*- coding:utf-8 -*-
# pip install locustio
# pip install websocket-client
from __future__ import absolute_import
from __future__ import unicode_literals
from __future__ import print_function

import json
import uuid
import time
import gevent

import random

from websocket import create_connection
import six

from locust import HttpLocust, TaskSet, task
from locust.events import request_success


class ChatTaskSet(TaskSet):
    chat_status = 'init'

    def on_start(self):
        self.user_id = six.text_type(uuid.uuid4())
        try:
            ws = create_connection('ws://46.101.154.244:8080/ws')
        except Exception, e:
            print("can't create connection")
            raise e
        self.ws = ws

        def _receive():
            while True:
                res = ws.recv()
                try:
                    data = json.loads(res)
                except Exception, e:
                    print(res)
                
                if data['command'] == 'wait':
                    self.chat_status = 'wait'
                elif data['command'] == 'startChat':
                    self.chat_status = 'chatting'
                elif data['command'] == 'stopChat':
                    self.chat_status == 'init'
                elif data['command'] == 'SendText':
                    try:
                        start_time = float(data['message'])
                        end_at = time.time()
                        request_success.fire(
                            request_type='WebSocket Recv',
                            name='test/ws/chat',
                            response_time=round((end_at-start_time)*1000),
                            response_length=len(res),
                        )
                    except Exception, e:
                        raise e

        gevent.spawn(_receive)

    def on_quit(self):
        self.ws.close()

    @task
    def sent(self):
        if self.chat_status == 'init':
            body = json.dumps({'command': 'MatchMeWithStranger'})
        elif self.chat_status == 'wait':
            body = ''
        elif self.chat_status == 'chatting':
            if random.randint(1, 50) == 4:
                msg = str(time.time())
                body = json.dumps({'command': 'SendText', 'message': msg})
            else:
                body = ''
        if body:
            self.ws.send(body)




class ChatLocust(HttpLocust):
    task_set = ChatTaskSet
    min_wait = 50
    max_wait = 100
