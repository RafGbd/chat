import asyncio
import time
import aiohttp_jinja2
import asyncio_redis
import jinja2
import settings
from aiohttp import web
from aiohttp_session import get_session, session_middleware
from aiohttp_session.cookie_storage import EncryptedCookieStorage
from aiopg.sa import create_engine
from app import views
from app import tasks
import os.path


POJECT_ROOT = os.path.dirname(os.path.realpath(__file__))

@asyncio.coroutine
def db_middleware(app, handler):
    """
        create database connection
    """
    @asyncio.coroutine
    def middleware(request):
        db = app.get('db')
        if not db:
            app['db'] = db = yield from create_engine(app['dsn'])
        request.app['db'] = db
        return (yield from handler(request))
    return middleware

@asyncio.coroutine
def redis_middleware(app, handler):
    """
        create redis connection
    """
    @asyncio.coroutine
    def middleware(request):
        redis = app.get('redis')
        if not redis:
            app['redis'] = redis = yield from asyncio_redis.Pool.create(host='127.0.0.1', port=6379, poolsize=20)
        request.app['redis'] = redis
        return (yield from handler(request))
    return middleware


# @asyncio.coroutine
def init(loop):
    """
        init aiohttp application and it's routers
    """
    app = web.Application(middlewares=[
        # session_middleware(EncryptedCookieStorage(settings.COOKIE_KEY)),
        # db_middleware,
        # redis_middleware
    ])
    app['dsn'] = 'postgres://%(db_user)s:%(db_password)s@%(db_host)s:5432/%(db_name)s' % {
        'db_user': settings.DB_USER,
        'db_name': settings.DB_NAME,
        'db_password': settings.DB_PASSWORD,
        'db_host': settings.DB_HOST,
    }

    aiohttp_jinja2.setup(
        app,
        loader=jinja2.FileSystemLoader(os.path.join(POJECT_ROOT, 'templates'))
    )

    app.router.add_static('/static', os.path.join(POJECT_ROOT, 'static'), name='static')
    app.router.add_route('GET', '/', views.index)
    app.router.add_route('GET', '/logout', views.logout)
    app.router.add_route('GET', '/login', views.login.get)
    app.router.add_route('POST', '/login', views.login.post)
    app.router.add_route('POST', '/change_logging', views.change_logging)
    app.router.add_route('GET', '/dashboard', views.dashboard)

    app.router.add_route('GET', '/_frontend', views.frontend)
    

    app.router.add_route('GET', '/ws', views.ws_handler)

    handler = app.make_handler()
    f = loop.create_server(handler, '0.0.0.0', 8081)
    srv = loop.run_until_complete(f)
    # return app, srv, handler
    return app

loop = asyncio.get_event_loop()

app = init(loop)
#app, srv, handler = init(loop)

loop.create_task(tasks.sync_redis_postgres_stats(app))
loop.create_task(views.message_dispatcher.run())


# if __name__ == '__main__':
#     print('serving on', srv.sockets[0].getsockname())
#     try:
#         loop.run_forever()
#     except KeyboardInterrupt:
#         pass
#     finally:
#         loop.run_until_complete(handler.finish_connections(5))
#         srv.close()
#         loop.run_until_complete(srv.wait_closed())
#         loop.run_until_complete(app.finish())
#     loop.close()
