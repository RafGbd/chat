import asyncio
import datetime
import asyncio_redis
import aiopg
import sqlalchemy as sa
import random
from dateutil.relativedelta import relativedelta
from aiopg.sa import create_engine
from .models import match_stats_table

# datetime.datetime.strptime(date, '%Y-%m-%d').date()

@asyncio.coroutine
def sync_redis_postgres_stats(app):
    """
        copy today users statistics data from redis to postgres
    """
    while True:
        today = datetime.date.today()
        today_str = today.strftime('%Y-%m-%d')
        redis = yield from asyncio_redis.Connection.create(host='127.0.0.1', port=6379)
        db = yield from create_engine(app['dsn'], minsize=5, maxsize=5)
        today_stats = yield from redis.hget('match_me_with_stranger_count', today_str)
        today_stats = today_stats or 0

        with (yield from db) as conn:
            query = yield from conn.execute(match_stats_table.select(
                match_stats_table.c.date == today
            ))
            row = yield from query.first()
            if row:
                yield from conn.execute(match_stats_table.update().where(
                    match_stats_table.c.date==today).values(match_me_count=today_stats))
                # update
            else:
                yield from conn.execute(match_stats_table.insert().values(
                    date=today,
                    match_me_count=today_stats
                ))
        redis.close()
        yield from asyncio.sleep(random.randint(15, 30))
