import asyncio
import aiohttp
import datetime
import distutils
import time
import uuid
import ujson
import asyncio_redis
import sqlalchemy as sa
from enum import Enum
from aiohttp import web
from aiohttp.log import ws_logger
from aiohttp_session import get_session
from aiohttp_jinja2 import render_template
from dateutil.relativedelta import relativedelta

from .models import (users_table, hash_password, chat_logs_table,
                     match_stats_table)
import logging

def login_required(handler):
    """
        decorator that redirects not authenticated users
        to /login page
    """
    @asyncio.coroutine
    def wrapped(request):
        session = yield from get_session(request)
        if session.get('is_authenticated'):
            return (yield from handler(request))
        else:
            return web.HTTPFound('/login')
    return wrapped


@asyncio.coroutine
def index(request):
    """
        redirects from / to /dashboard
    """
    return web.HTTPFound('/dashboard')


@login_required
@asyncio.coroutine
def dashboard(request):
    """
        dashboard page
    """
    def build_aggr_query(start_date):
        """
            build aggregation query to count number of match_me_with_stranger
            calls from start_date to now
        """
        return (sa.select([sa.func.sum(match_stats_table.c.match_me_count)])
                .where(match_stats_table.c.date >= start_date)
                .select_from(match_stats_table))

    today = datetime.date.today()
    today_str = today.strftime('%Y-%m-%d')
    redis = request.app['redis']

    today_stats = yield from redis.hget('match_me_with_stranger_count', today_str)
    today_stats = today_stats or 0

    with (yield from request.app['db']) as conn:
        """
            count number of match_me_with_stranger_count for different
            time intervals
        """
        query_last_week = build_aggr_query(today - relativedelta(days=7))
        match_me_last_week = (yield from conn.scalar(query_last_week))

        query_last_month = build_aggr_query(today - relativedelta(months=1))
        match_me_last_month = (yield from conn.scalar(query_last_month))

        query_last_three_months = build_aggr_query(today - relativedelta(months=3))
        match_me_last_three_months = (yield from conn.scalar(query_last_three_months))

        query_last_six_months = build_aggr_query(today - relativedelta(months=6))
        match_me_last_six_months = (yield from conn.scalar(query_last_six_months))

        query_last_year = build_aggr_query(today - relativedelta(months=12))
        match_me_last_year = (yield from conn.scalar(query_last_year))

        match_me_with_stranger = {
            "last_week": str(match_me_last_week),
            "last_month": str(match_me_last_month),
            "last_three_months": str(match_me_last_three_months),
            "last_six_months": str(match_me_last_six_months),
            "last_year": str(match_me_last_year),
        }

    match_me_with_stranger.update({'today': today_stats})

    users_stats_d = yield from redis.hgetall('users_stats')
    users_stats = yield from users_stats_d.asdict()

    logging_enabled = yield from redis.hget('settings', 'logging_enabled')
    logging_enabled =  distutils.util.strtobool(logging_enabled or 'False')

    return render_template('dashboard.html', request, {
        'match_me_with_stranger': match_me_with_stranger,
        'users_stats': users_stats,
        'logging_enabled': logging_enabled,
    })


@login_required
@asyncio.coroutine
def change_logging(request):
    """
        enable\disable logging
    """
    data = yield from request.post()
    logging_enabled = 'logging_enabled' in data
    logging_enabled = str(logging_enabled)
    yield from request.app['redis'].hset(
        'settings', 'logging_enabled', logging_enabled)
    return web.HTTPFound('/')


@asyncio.coroutine
def logout(request):
    """
        logout
    """
    session = yield from get_session(request)
    session['is_authenticated'] = False
    return web.HTTPFound('/')

@asyncio.coroutine
def frontend(request):
    """
        frontend page example
    """
    return render_template('frontend.html', request, {})

class LoginView:
    @asyncio.coroutine
    def get(self, request):
        """
            returns login page
        """
        return render_template('login.html', request, {'error': False})

    @asyncio.coroutine
    def post(self, request):
        """
            process submitted login form
        """
        data = yield from request.post()
        # redirect to index
        username = data.get('username', '')
        password = data.get('password', '')

        with (yield from request.app['db']) as conn:
            query = users_table.select(users_table.c.username == username)
            user = yield from (yield from conn.execute(query)).first()
            if user and user.password == hash_password(password):
                session = yield from get_session(request)
                session['is_authenticated'] = True
                return web.HTTPFound('/')
            else:
                return render_template('login.html', request, {'error': True})

login = LoginView()

class WsUser():
    def __init__(self, uid=None, server_uid=None, ws_uid=None):
        assert (uid or (server_uid and ws_uid))
        if uid:
            self.uid = uid
            self.server_uid, self.ws_uid = uid.split("_")
        elif server_uid and ws_uid:
            self.uid = "_".join([server_uid, ws_uid])
            self.server_uid = server_uid
            self.ws_uid = ws_uid

    def is_local(self, server_uid):
        return self.server_uid == server_uid

class MessageDispathcer():
    def __init__(self):
        self.server_uid = str(uuid.uuid4())
        self.connections = {}

    def add_connection(self, ws_connection):
        self.connections[ws_connection.ws_uid] = ws_connection

    def remove_connction(self, ws_connection):
        del self.connections[ws_connection.ws_uid]
    
    @asyncio.coroutine
    def send(self, message, receiver):
        """
            send message directrly to local connection
            or to remote server redis channel
        """
        if receiver.ws_uid in self.connections:
            yield from self.connections[receiver.ws_uid].receive_message(message)
        else:
            message["receiver_uid"] = receiver.uid
            yield from self.redis.protocol.publish(receiver.server_uid, ujson.dumps(message))

    @asyncio.coroutine
    def run(self):
        self.redis = yield from asyncio_redis.Pool.create(
            host='127.0.0.1', port=6379, poolsize=10)
        self.subscriber = yield from self.redis.start_subscribe()
        yield from self.subscriber.subscribe([self.server_uid])
        yield from self.handle_income()

    @asyncio.coroutine
    def handle_income(self):
        """process messages from redis"""
        while True:
            msg_redis = yield from self.subscriber.next_published()
            if msg_redis:
                message = ujson.loads(msg_redis.value)
                receiver = WsUser(uid=message['receiver_uid'])
                if receiver.ws_uid in self.connections:
                    yield from self.connections[receiver.ws_uid].receive_message(message)


message_dispatcher = MessageDispathcer()


        
class WsStatus(Enum):
    """
        statuses of WebSocket connection
    """
    initializing = 0
    # user just opened ws connection or stranger stopped chat
    waiting = 1
    # waiting for stranger
    waiting_for_hs = 2
    # waiting for stranger to response chat invite (handshake)
    chatting = 3
    # chatting

class WebsocketHandler:
    """
        main class to process WebSocket connection
    """
    def __init__(self, message_dispatcher):
        self.message_dispatcher = message_dispatcher
        self.logging_enabled = False
        self.stranger = None
        self.ws_uid = str(uuid.uuid4())
        self.user = WsUser(
            server_uid=self.message_dispatcher.server_uid,
            ws_uid=self.ws_uid)
        self.status = WsStatus.initializing

    #
    # COMMON SECTION
    #


    @asyncio.coroutine
    def set_status(self, status):
        """
            set user status and changes counters
        """
        if status == WsStatus.chatting and self.status != WsStatus.chatting:
            yield from self.redis.hincrby('users_stats', 'users_chatting', 1)
        if status != WsStatus.chatting and self.status == WsStatus.chatting:
            yield from self.redis.hincrby('users_stats', 'users_chatting', -1)
        self.status = status

    @asyncio.coroutine
    def handle(self, request):
        """
            handles websocket connection and create redis subscription
        """
        self.ws = web.WebSocketResponse()
        yield from self.ws.prepare(request)

        # self.db = request.app['db']
        self.redis = yield from asyncio_redis.Pool.create(host='127.0.0.1', port=6379, poolsize=2)
        
        logging_enabled = yield from self.redis.hget('settings', 'logging_enabled')
        self.logging_enabled =  distutils.util.strtobool(logging_enabled or 'False')

        try:
            yield from self.redis.hincrby('users_stats', 'users_online', 1)
            yield from self.handle_ws()
        except Exception as e:  # Don't do except: pass
            import traceback
            traceback.print_exc()
        finally:
            yield from self.clean_up()
            self.redis.close()
        return self.ws

    @asyncio.coroutine
    def clean_up(self):
        """
         cleaning up after user disconnect
        """
        # decreasing counters
        try:
            yield from self.set_status(WsStatus.initializing)
            yield from self.redis.hincrby('users_stats', 'users_online', -1)
        except Exception as e:
            print(e)
        # send stop to stranger
        try:
            yield from self.stop_chatting(reason="disconnect")
        except Exception as e:
            print(e)

        # close ws if it's possible
        try:
            yield from self.ws.close()
        except Exception as e:
            print(e)

        # remove self from queue if it's necessary
        if self.status == WsStatus.waiting:
            try:
                yield from self.redis.srem(self.queue, [self.user.uid])
            except Exception as e:
                print(e)
        # decrease counters

    #
    # WEBSCOCKET SECTION
    #

    async def handle_ws(self):
        """process messages from WebSocket"""

        async for msg in self.ws:
            if msg.tp == aiohttp.MsgType.text:
                try:
                    msg_data = ujson.loads(msg.data)
                    command = msg_data['command']
                    if command == 'MatchMeWithStranger':
                        self.premium = msg_data.get('premium', 0)
                        await self.match_me_with_stranger(premium=self.premium)
                    elif command == 'SendText':
                        message = msg_data.get("message", '')
                        await self.ws_send_text(message=message)

                except (ValueError, KeyError) as e:
                    raise e
                    error = {"command": "error"}
                    self.ws.send_str(ujson.dumps(error))
                # process commands
            elif msg.tp == aiohttp.MsgType.error:
                raise ws.exception()

    @asyncio.coroutine
    def check_handshake_ok(self):
        """
            if stranger don't reply to startChat, we removing him from queue
            and trying match_me_with_stranger again
        """
        yield from asyncio.sleep(4)
        if self.status == WsStatus.waiting_for_hs and self.stranger:
            if self.stranger:
                yield from self.redis.srem('premium_queue', [self.stranger.uid])
            if self.stranger:
                yield from self.redis.srem('regular_queue', [self.stranger.uid])
            yield from self.stop_chatting(reason='timeout')
            yield from self.match_me_with_stranger()

    @asyncio.coroutine
    def get_stranger_from_queue(self):
        """
            get stranger from queue.
            premium first, regular second
        """
        premium_queue_result = yield from self.redis.srandmember('premium_queue')
        strangers_set = yield from premium_queue_result.asset()
        if not strangers_set:
            regular_queue_result = yield from self.redis.srandmember('regular_queue')
            strangers_set = yield from regular_queue_result.asset()
        if strangers_set:
            return WsUser(uid=strangers_set.pop())

    @asyncio.coroutine
    def match_me_with_stranger(self, premium=0):
        """
            getting stranger from queue if there's any and sending him startChat
            command.

            if there's not strangers in queue, going to queue
        """
        if self.status == WsStatus.waiting:
            yield from self.redis.srem(self.queue, [self.user.uid])
        if self.status == WsStatus.chatting:
            yield from self.stop_chatting(reason='match_me_with_stranger')

        today = datetime.date.today().strftime('%Y-%m-%d')
        yield from self.redis.hincrby('match_me_with_stranger_count', today, 1)
        stranger = yield from self.get_stranger_from_queue()

        if stranger:
            # sending invite
            self.stranger = stranger
            yield from self.set_status(WsStatus.waiting_for_hs)
            self.stranger = stranger
            start_msg = {
                "command": "startChat",
                "user_uid": self.user.uid,
                "premium": self.premium,
            }
            yield from self.message_dispatcher.send(message=start_msg, receiver=stranger)
            yield from self.check_handshake_ok()
        else:
            # going to queue
            self.queue = 'premium_queue' if premium else 'regular_queue'
            yield from self.redis.sadd(self.queue, [self.user.uid])
            yield from self.set_status(WsStatus.waiting)
            wait_msg = ujson.dumps({"command": "wait"})
            self.ws.send_str(wait_msg)


    @asyncio.coroutine
    def stop_chatting(self, reason):
        """
            stop chat with current stranger
        """
        ws_logger.info("stop_chatting, reason %s" % reason)
        if self.stranger:
            stop_msg = {
                "command": "Stop",
                "user_uid": self.user.uid
            }
            yield from self.message_dispatcher.send(receiver=self.stranger, message=stop_msg)
            self.stranger = None
            yield from self.set_status(WsStatus.initializing)

    @asyncio.coroutine
    def ws_send_text(self, message):
        """
            send message to current stranger
        """
        if self.status == WsStatus.chatting and self.stranger:
            send_msg = {
                "command": "SendText",
                "message": message,
                "user_uid": self.user.uid
            }
            yield from self.message_dispatcher.send(receiver=self.stranger, message=send_msg)
            if self.logging_enabled:
                with (yield from self.db) as conn:
                    yield from conn.execute(chat_logs_table.insert().values(
                        sender_uid=self.user.uid,
                        receiver_uid=self.stranger.uid,
                        message=message,
                        datetime=datetime.datetime.utcnow()
                    ))
    #
    # REDIS SECTION
    #

    # @asyncio.coroutine
    # def handle_redis(self):
    #     """process messages from redis"""
    #     while True:
    #         msg_redis = yield from self.subscriber.next_published()
    #         if msg_redis:
    #             try:
    #                 msg_data = ujson.loads(msg_redis.value)
    #                 command = msg_data['command']
    #                 stranger_uid = msg_data['user_uid']

    #                 if command == 'startChat':
    #                     stranger_premium = msg_data['premium']
    #                     yield from self.redis_start_chat(stranger_uid, stranger_premium)

    #                 elif command == 'startError':
    #                     yield from self.redis_start_error()

    #                 elif command == 'startOK':
    #                     stranger_premium = msg_data['premium']
    #                     yield from self.redis_start_ok(stranger_uid, stranger_premium)

    #                 elif command == 'Stop':
    #                     yield from self.redis_stop(stranger_uid)

    #                 elif command == 'SendText':
    #                     text = msg_data.get('message', '')
    #                     yield from self.redis_send_text(stranger_uid, text)

    #             except (ValueError, KeyError):
    #                 error = {"command": "error"}

    @asyncio.coroutine
    def receive_message(self, message):
        try:
            command = message['command']
            
            stranger = WsUser(uid=message['user_uid'])

            if command == 'startChat':
                stranger_premium = message['premium']
                yield from self.receive_start_chat(stranger, stranger_premium)

            elif command == 'startError':
                yield from self.receive_start_error()

            elif command == 'startOK':
                stranger_premium = message['premium']
                yield from self.receive_start_ok(stranger, stranger_premium)

            elif command == 'Stop':
                yield from self.receive_stop(stranger)

            elif command == 'SendText':
                text = message.get('message', '')
                yield from self.receive_send_text(stranger, text)

        except Exception as e:
            raise e
            error = {"command": "error"}

    @asyncio.coroutine
    def receive_start_chat(self, stranger, stranger_premium):
        """
            process invite from stranger to start chat.
            answer startOK if status is 'waiting',
            else answer startError 
        """
        if self.status == WsStatus.waiting and self.stranger == None:
            self.stranger = stranger
            yield from self.set_status(WsStatus.chatting)

            yield from self.redis.srem(self.queue, [self.user.uid])

            ws_start_chat_msg = ujson.dumps({
                "command": "startChat",
                "stranger_premium": stranger_premium,
            })
            self.ws.send_str(ws_start_chat_msg)

            ok_msg = {
                "command": "startOK",
                "user_uid": self.user.uid,
                "premium": self.premium
            }
            yield from self.message_dispatcher.send(
                receiver=self.stranger, message=ok_msg)
        else:
            error_msg = {
                "command": "startError",
                "user_uid": self.user.uid
            }
            yield from self.message_dispatcher.send(
                receiver=stranger, message=error_msg)

    @asyncio.coroutine
    def receive_start_error(self):
        """
            we invited stranger to start chat, but he was already busy
            calling match_me_with_stranger again
        """
        self.stranger = None
        yield from self.set_status(WsStatus.initializing)
        yield from self.match_me_with_stranger()

    @asyncio.coroutine
    def receive_start_ok(self, stranger, stranger_premium):
        """
            stranger accepted our invite
        """
        if self.stranger and self.stranger.uid == stranger.uid:
            yield from self.set_status(WsStatus.chatting)
            ws_start_chat_msg  = ujson.dumps({
                "command": "startChat",
                "stranger_premium": stranger_premium
            })
            self.ws.send_str(ws_start_chat_msg)

    @asyncio.coroutine
    def receive_stop(self, stranger):
        """
            stranger stopped chat with us
        """
        if self.stranger and self.stranger.uid == stranger.uid:
            self.stranger == None
            yield from self.set_status(WsStatus.initializing)
            ws_stop_chat_msg = ujson.dumps({"command": "stopChat"})
            self.ws.send_str(ws_stop_chat_msg)

    @asyncio.coroutine
    def receive_send_text(self, stranger, text):
        """
            stranger sended message to us
            check starnger's uid and send message to websocket
        """
        if self.stranger and self.stranger.uid == stranger.uid:
            ws_send_text_msg = ujson.dumps({
                "command": "SendText",
                "message": text,
            })
            self.ws.send_str(ws_send_text_msg)

@asyncio.coroutine
def ws_handler(request):
    """
    create handler for each connection
    """
    websocket_handler = WebsocketHandler(message_dispatcher)
    message_dispatcher.add_connection(websocket_handler)
    return (yield from websocket_handler.handle(request))
