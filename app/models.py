import base64
from datetime import datetime
from sqlalchemy.schema import CreateTable
import sqlalchemy as sa
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes

metadata = sa.MetaData()

"""
    Definitoin of Users Table
"""
users_table = sa.Table('users', metadata,
    sa.Column('id', sa.Integer, primary_key=True),
    sa.Column('username', sa.String(255), unique=True, nullable=False),
    sa.Column('password', sa.String(255), nullable=False),
    sa.Column('created', sa.DateTime, default=datetime.now)
)

def hash_password(value):
    """
        creates hashed password
    """
    if isinstance(value, str):
        value = str.encode(value)
    digest = hashes.Hash(hashes.SHA256(), backend=default_backend())
    digest.update(value)
    return base64.b64encode(digest.finalize()).decode()


"""
    Definitoin of MatchMeWithStranger dayly stats table
"""
match_stats_table = sa.Table('match_stats', metadata,
    sa.Column('date', sa.Date, primary_key=True),
    sa.Column('match_me_count', sa.Integer),
)

"""
    Definitoin of Message logs table
"""
chat_logs_table = sa.Table('chat_logs', metadata,
    sa.Column('id', sa.Integer, primary_key=True),
    sa.Column('sender_uid', sa.String(36), nullable=False),
    sa.Column('receiver_uid', sa.String(36), nullable=False),
    sa.Column('message', sa.Text(), nullable=False),
    sa.Column('datetime', sa.DateTime, default=datetime.now),
)